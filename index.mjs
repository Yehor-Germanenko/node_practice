import * as utils1 from 'test-utils';
import utils from 'util';

const params = { password: 'sdf234fdsf32cdsc' };

const myFuncPromise = utils.promisify(utils1.runMePlease);

try {
    const result = await myFuncPromise(params);
    console.log(result);
} catch (error){
    console.log(error);
}